tkk_MyCheckout
bastian charlet <b.charlet@tk-kommunikation.de>
(c) tk-kommunikation

Tkk_MyCheckout

Entfernt den Schritt Versandarten aus dem OnePageCheckout. Es muss die Templatedatei "checkout/onepage.phtml" angepasst werden, indem der Schritt entfernt wird.