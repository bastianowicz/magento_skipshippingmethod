<?php
/**
 *
 * @category    Tkk
 * @package     Tkk_MyCheckout
 * @copyright   Copyright (c) tk-kommunikation
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */


require_once Mage::getModuleDir('controllers', 'Mage_Checkout').DS.'OnepageController.php';

class Tkk_MyCheckout_OnepageController extends Mage_Checkout_OnepageController
{
     /**
     * Lenkt den Request auf die Zahlungsmethoden um
     * So wird der Schritt 'Versandarten' �bersprungen
     * @author Bastian Charlet <b.charlet@tk-kommunikation.de>
     * @see Mage_Checkout_OnepageController::saveShippingAction()
     * @override 
     */
    public function saveBillingAction()
    {
        if ($this->_expireAjax()) {
            return;
        }
        $this->setShippingCommentToQuote();
        if ($this->getRequest()->isPost()) {
//            $postData = $this->getRequest()->getPost('billing', array());
//            $data = $this->_filterPostData($postData);
            $data = $this->getRequest()->getPost('billing', array());
            $customerAddressId = $this->getRequest()->getPost('billing_address_id', false);

            if (isset($data['email'])) {
                $data['email'] = trim($data['email']);
            }
            $result = $this->getOnepage()->saveBilling($data, $customerAddressId);

            
            if (!isset($result['error'])) {
            
            	$method = 'freeshipping_freeshipping';
            	$result = $this->getOnepage()->saveShippingMethod($method);
            	Mage::getSingleton('checkout/type_onepage')->getQuote()->getShippingAddress()->setShippingMethod($method)->save();
            }
            	
            if (!isset($result['error'])) {
                /* check quote for virtual */
                if ($this->getOnepage()->getQuote()->isVirtual()) {
                    $result['goto_section'] = 'payment';
                    $result['update_section'] = array(
                        'name' => 'payment-method',
                        'html' => $this->_getPaymentMethodsHtml()
                    );
                } elseif (isset($data['use_for_shipping']) && $data['use_for_shipping'] == 1) {
                        $result['goto_section'] = 'payment';
                        $result['update_section'] = array(
                        'name' => 'payment-method',
                        'html' => $this->_getPaymentMethodsHtml()
                        );

                    $result['allow_sections'] = array('shipping');
                    $result['duplicateBillingInfo'] = 'true';
                } else {
                    $result['goto_section'] = 'shipping';
                }
            }

            $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($result));
        }
    }

    /**
     * Lenkt den Request auf die Zahlungsmethoden um
     * So wird der Schritt 'Versandarten' �bersprungen
     * @author Bastian Charlet <b.charlet@tk-kommunikation.de>
     * @see Mage_Checkout_OnepageController::saveShippingAction()
     * @override 
     */
    public function saveShippingAction()
    {
        if ($this->_expireAjax()) {
            return;
        }
        if ($this->getRequest()->isPost()) {
            $data = $this->getRequest()->getPost('shipping', array());
            $customerAddressId = $this->getRequest()->getPost('shipping_address_id', false);
            $result = $this->getOnepage()->saveShipping($data, $customerAddressId);
            

        if (!isset($result['error'])) {
			$result['goto_section'] = 'payment';
			$result['update_section'] = array(
				'name' => 'payment-method',
				'html' => $this->_getPaymentMethodsHtml()
			);
		}
            $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($result));
        }
    }
    
    
    /**
     * Speichert die �bergebene Information zum gew�nschten kontakttermin in der Session
     * @author bastian charlet <b.charlet@tk-kommunikation.de>
     */
    public function setShippingCommentToQuote()
    {
    	$orderComment = $this->getRequest()->getPost('contact_date', false);
    		if(empty($orderComment)) return false;
    	
    	$session = Mage::getSingleton('checkout/session');
    	$session->setData('contact_date', $orderComment);
    }
    
    
    /**
     * Speichert die Daten zur Kontaktzeit aus der Session im Auftrag (falls vorhanden)
     * wird von einem entsprechenden Observer aufgerufen
     * 
     * @author bastian charlet <b.charlet@tk-kommunikation.de>
     * @param Varien_Event_Observer $observer
     */
    public function setShippingCommentToOrder(Varien_Event_Observer $observer)
    {
    	$comment = Mage::getSingleton('checkout/session')->getData('contact_date');
    
    	if( !empty( $comment ) ){
	    	$contactDate = "Kontaktaufnahme gewünscht: " . $comment;
    		$observer->getEvent()->getOrder()->addStatusHistoryComment($contactDate);
    	}
    }
}
